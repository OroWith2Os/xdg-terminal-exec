use serde::Deserialize;
use std::env;
use std::fs;
use toml::Value;

fn read_configs() {}

#[derive(Deserialize)]
pub struct Config {
    pub terminals: Vec<String>,
}

/// Read the specified TOML configuration file from the input path
/// and return the configuration.
fn read_config(filename: String) -> Config {
    let file_contents = match fs::read_to_string(filename) {
        Ok(c) => c,
        Err(_) => panic!("No valid configuration file was found."),
    };

    let value = file_contents
        .parse::<Value>()
        .expect("Failed to parse TOML");

    value.try_into().expect("Failed to deserialize TOML")
}

/// Get the current name of the desktop.
fn desktop_name() -> Result<String, String> {
    let desktop = env::var("XDG_CURRENT_DESKTOP");
    match desktop {
        Ok(val) => Ok(val),
        Err(_) => Err("No configuration file was available for the desktop.".to_owned()),
    }
}
