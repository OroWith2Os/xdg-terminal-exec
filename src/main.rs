use clap::Parser;
use std::process::exit;

mod configuration;
mod terminal;

#[derive(Parser)]
#[clap(author, version, about)]
struct Cli {
    /// Configure xdg-terminal-exec
    #[arg(long)]
    config: bool,

    /// The command to run inside of the terminal emulator
    #[arg(trailing_var_arg = true)]
    command: Vec<String>,
}

fn main() {
    let args = Cli::parse();

    #[allow(unreachable_code)]
    if args.config {
        todo!("Need to implement CLI/TUI setup.");
        exit(0);
    }

    if !args.command.is_empty() {
        let command = shell_words::join(args.command);
    }
}
