use std::process::Command;
use which::which;

use crate::configuration::Config;

fn config_to_terminalvec(config: Config) -> Vec<Terminal> {
    config
        .terminals
        .iter()
        .map(|terminal_str| {
            let parts = shell_words::split(terminal_str).expect("Failed to split terminal string");
            let binary = parts
                .get(0)
                .cloned()
                .expect("Invalid terminal string: no binary provided");
            let args = parts[1..].to_vec();
            Terminal { binary, args }
        })
        .collect()
}

/// Individual terminal binaries
pub struct Terminal {
    pub binary: String,
    pub args: Vec<String>,
}

impl Terminal {
    /// Verify the terminal binary exists and is executable,
    /// and if not, return an error.
    pub fn verify_binary(&self) -> Result<Terminal, String> {
        let mut vec: Vec<String> = shell_words::split(&self.binary)
            .expect("Failed to split terminal command from configuration.");

        let args = vec.split_off(1);
        let binary = vec.remove(0);

        which(&binary)
            .map(|_| Terminal { binary, args })
            .map_err(|_| "Terminal binary is not valid.".to_string())
    }
    /// Spawn the terminal emulator with the specified command.
    pub fn spawn(&self, command: String) {
        Command::new(&self.binary).args(&self.args).arg(command);
    }
}
